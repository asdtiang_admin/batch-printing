/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.asdtiang.asdtiangtools;

import java.io.File;
import java.util.Date;

/**
 * 
 * @author asdtiang asdtiangxia@163.com
 *
 * 2018年8月16日
 */
public class PrintItem {

    File file;
    PrintStatus printStatus;

    public enum PrintStatus {
        PRINTED_SUCCESS("打印成功"), PRINTED_ERROR("打印出错"), PRINTING("正在打印"), WILL_PRINT("打印排队");
        String value;

        PrintStatus(String v) {
            this.value = v;
        }

        public String getValue() {
            return value;
        }
    }
}
