/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.asdtiang.asdtiangtools;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * 
 * @author asdtiang asdtiangxia@163.com
 *
 * 2018年8月16日
 */
public class PrintTableModel extends AbstractTableModel {

    private List<PrintItem> printList;
    private int column;
    private String[] columnNames;
    java.text.DateFormat format = new java.text.SimpleDateFormat("hh:mm:ss");

    public PrintTableModel(List<PrintItem> printList) {
        columnNames = new String[]{"文件名", "路径","状态"};
        this.printList = printList;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return printList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        PrintItem item = this.printList.get(rowIndex);
        String result = "";
        if (columnIndex == 0) {
            result = item.file.getName();
        }
        if (columnIndex == 1) {
            result = item.file.getAbsolutePath();
        }
        if (columnIndex == 2) {
            result = item.printStatus.getValue();
        }
        return result;
    }

}
