/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.asdtiang.asdtiangtools;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
/**
 * 
 * @author asdtiang asdtiangxia@163.com
 *
 * 2018年8月16日
 */
public class PrintWork {
    public static PDDocument document;
    public static void print(File file) throws IOException, PrinterException {
            PDDocument document = PDDocument.load(file);
            print(document);
            document.close();
    }
    private static void print(PDDocument document) throws IOException, PrinterException
    {
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPageable(new PDFPageable(document));
        job.print();
    }
    
}
